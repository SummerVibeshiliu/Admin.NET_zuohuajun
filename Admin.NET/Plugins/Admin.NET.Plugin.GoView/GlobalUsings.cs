﻿// 大名科技（天津）有限公司 版权所有
//
// 此源代码遵循位于源代码树根目录中的 LICENSE 文件的许可证
//
// 不得利用本项目从事危害国家安全、扰乱社会秩序、侵犯他人合法权益等法律法规禁止的活动
//
// 任何基于本项目二次开发而产生的一切法律纠纷和责任，均与作者无关

global using Admin.NET.Core;
global using Admin.NET.Core.Service;
global using Furion;
global using Furion.DatabaseAccessor;
global using Furion.DataValidation;
global using Furion.DynamicApiController;
global using Furion.FriendlyException;
global using Furion.JsonSerialization;
global using Furion.UnifyResult;
global using Mapster;
global using Microsoft.AspNetCore.Authorization;
global using Microsoft.AspNetCore.Http;
global using Microsoft.AspNetCore.Mvc;
global using Microsoft.AspNetCore.Mvc.Filters;
global using Newtonsoft.Json;
global using SqlSugar;
global using System.Collections;
global using System.ComponentModel;
global using System.ComponentModel.DataAnnotations;
global using System.Data;
global using System.Linq.Dynamic.Core;